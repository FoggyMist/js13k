# js13k game - Join the cubes

A game made for [js13k](https://js13kgames.com).

## How the game is bundled

Install dependencies with: `npm ci`.
NPM version used: 6.14.4.

Transpile TS to JS: `npm run tsc`.

Minify JS files: `npm run gulp`.

Extract `/index.html` and `/bin/*.js` files to separated directory and zip them.

